import React from 'react';
import { Switch, Route, NavLink } from 'react-router-dom';
import Home from './home';
import About from './about';
import Posts from './posts';
import Post from './post';

class App extends React.Component {
  render() {
    return (
      <div>
        <div>
          <nav className="navbar">
            <h1>
              <NavLink exact to="/">Angular Routing</NavLink>
            </h1>
            <ul>
              <li>
                <NavLink exact to="/">Home</NavLink>
              </li>
              <li>
                <NavLink to="/posts">Posts</NavLink>
              </li>
              <li>
                <NavLink to="/about">About</NavLink>
              </li>
              <li>
                <button className="btn">Login</button>
              </li>
              <li>
                <button className="btn">Logout</button>
              </li>
            </ul>
          </nav>
        </div>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/about" exact component={About} />
          <Route path="/posts" exact component={Posts} />
          <Route path="/posts/:id" exact component={Post} />
        </Switch>
      </div>
    );
  }
}

export default App;
