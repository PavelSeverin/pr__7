import React, { Component } from 'react';
import { Link } from 'react-router-dom';

const posts = [
  { title: 'Post 1', text: 'Sample text for post 1', id: 11 },
  { title: 'Post 2', text: 'Sample text for post 2', id: 22 },
  { title: 'Post 3', text: 'Sample text for post 3', id: 33 },
  { title: 'Post 4', text: 'Sample text for post 4', id: 44 },
];

export const getById = (id) => {
  return posts.find((p) => p.id === id);
};

export default class Posts extends Component {
  render() {
    return (
      <div className="container card">
        <h1>Posts page</h1>
        {posts.map((p) => (
          <div key={p.id} className="card">
            <Link to={`/posts/${p.id}`}>#{p.id} - {p.title}</Link>
          </div>
        ))}
      </div>
    );
  }
}
