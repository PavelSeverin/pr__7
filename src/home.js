import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

class Home extends Component {
  openPosts = () => this.props.history.push('/posts');
  render() {
    return (
      <div className="container">
        <h1>Home page</h1>
        <button className="btn" onClick={this.openPosts}>
          Go to posts
        </button>
      </div>
    );
  }
}

export default withRouter(Home);
