import React, { Component } from 'react';
import { getById } from './posts';

export default class Post extends Component {
  constructor(p) {
    super(p);
    const { params } = this.props.match;
    const id = Number(params.id);
    this.state = { post: getById(id) };
  }
  render() {
    const { history } = this.props;
    const { post } = this.state;
    return (
      <div className="container">
        <button className="btn" onClick={() => history.goBack()}>
          Back
        </button>
        <div className="card">
          <h1>
            Post {post.id} - {post.title}
          </h1>
          <div>Post {post.text}</div>
        </div>
        <button
          className="btn btn-danger"
          onClick={() => alert(`I don't know what it's meant to be :/`)}
        >
          Load 4 posts
        </button>
      </div>
    );
  }
}
